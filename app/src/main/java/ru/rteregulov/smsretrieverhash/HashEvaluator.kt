package ru.rteregulov.smsretrieverhash

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.groups.mutuallyExclusiveOptions
import com.github.ajalt.clikt.parameters.groups.required
import com.github.ajalt.clikt.parameters.groups.single
import com.github.ajalt.clikt.parameters.options.convert
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.prompt
import com.github.ajalt.clikt.parameters.types.file
import java.io.File
import java.security.KeyStore
import java.security.MessageDigest
import java.util.Base64

fun main(args: Array<String>) {
    HashEvaluator().main(args)
}

private const val HEX_ARRAY = "0123456789abcdef"

class HashEvaluator : CliktCommand() {
    private val keystore: KeystoreData by mutuallyExclusiveOptions<KeystoreData>(
            option("--keystoreBase64", help = "Base64 represented raw keystore")
                    .convert("BASE64") { KeystoreData.Base64Keystore(it) },
            option("--keystoreFilePath", help = "Path to keystore")
                    .file(mustExist = true, mustBeReadable = true)
                    .convert("FILE") { KeystoreData.KeystorePath(it) }
    ).single().required()

    private val keystorePassword: String by option(help = "Keystore password").prompt(
            "Enter keystore password",
            hideInput = true
    )
    private val keyAlias: String by option(help = "Key alias").prompt("Your key alias")
    private val packageName: String by option(help = "Your app package").prompt("You app package")

    override fun run() {
        val keystoreByteArray = when (val ks = keystore) {
            is KeystoreData.Base64Keystore -> Base64.getDecoder().decode(ks.raw)
            is KeystoreData.KeystorePath -> ks.file.readBytes()
        }

        val keyStore = KeyStore.getInstance("jks")
        keyStore.load(keystoreByteArray.inputStream(), keystorePassword.toCharArray())

        val cert = keyStore.getCertificate(keyAlias)
        val hexCert = encodeToHex(cert.encoded)
        val stringForHash = "$packageName $hexCert"

        val digest = MessageDigest.getInstance("SHA-256")

        val sha256 = digest.digest(stringForHash.trim().toByteArray())
        val base64 = Base64.getEncoder().encodeToString(sha256)

        println("Result hash-string: ${base64.take(11)}")
    }

    private fun encodeToHex(bytes: ByteArray): String {
        val hexChars = CharArray(bytes.size * 2)

        for (i in bytes.indices) {
            val firstCharIndex = i * 2
            val byte = bytes[i].toInt() and 0xFF
            hexChars[firstCharIndex] = HEX_ARRAY[byte ushr 4]
            hexChars[firstCharIndex + 1] = HEX_ARRAY[byte and 0x0F]
        }
        return String(hexChars)
    }
}

sealed class KeystoreData {
    class Base64Keystore(val raw: String) : KeystoreData()
    class KeystorePath(val file: File) : KeystoreData()
}